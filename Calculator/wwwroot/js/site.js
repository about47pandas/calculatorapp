﻿const operatorRegex = /(x|\/|\+|\-)/gi;
var calcInput = document.getElementById('calculator-input');

var outputWrapper = document.getElementById('calc-button-wrapper');
var outputSpan = document.getElementById('calc-output');

document.querySelectorAll(".calc-btn")
    .forEach(el => {
        el.onclick = function () {
            calcInput.value += this.getAttribute('data-value');
        };
    });


function calculate() {

    try {
        outputSpan.innerHTML = '';
        let originalInput = calcInput.value.replace(/ /g);

        let inputs = originalInput.split(operatorRegex);
        inputs = inputs.map(val => parseFloat(val));

        let operators = originalInput.match(operatorRegex);

        let calculatedValue = applyOperation(inputs[0], inputs[1], operators[1]);

        for (let i = 0; i < inputs.length - 1; i++) {
            calculatedValue += applyOperation(calculatedValue, inputs[i + 1], operators[i + 1]);
        }

        outputSpan.innerHTML = calculatedValue;
        outputWrapper.removeClass('hidden');
    }
    catch (e) {
        outputWrapper.addClass('hidden');
        console.error(e);
        throw new Error(e);
    }
}

function applyOperation(value1, value2, operator) {
    switch (operator) {
        case /x/i:
            return value1 * value2;
        case '/':
            return value1 / value2;
        case '+':
            return value1 + value2;
        case '-':
            return value1 - value2;
    }
}